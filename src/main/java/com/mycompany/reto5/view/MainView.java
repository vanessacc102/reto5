package com.mycompany.reto5.view;

import javax.swing.JOptionPane;

public class MainView extends javax.swing.JFrame {
   
    public MainView() {
        initComponents();
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
    }

    private void cerrar(){
        String botones[] = {"Cerrar","Cancelar"};
        int eleccion = JOptionPane.showOptionDialog(this,"¿Desea cerrar la aplicación?","ADVERTENCIA",
                        0,0,null,botones,this);
        if(eleccion == JOptionPane.YES_OPTION){
            System.exit(0);
        } else if(eleccion == JOptionPane.NO_OPTION){
            System.out.println("Se cancelo el cierre");
        }
        
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        txtReto5 = new javax.swing.JLabel();
        txtEnunciado = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        btnInforme3 = new javax.swing.JButton();
        btnInforme1 = new javax.swing.JButton();
        btnInforme2 = new javax.swing.JButton();
        Icon3 = new javax.swing.JLabel();
        Icon1 = new javax.swing.JLabel();
        Icon2 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(56, 28, 64));
        jPanel1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txtReto5.setBackground(new java.awt.Color(204, 204, 255));
        txtReto5.setFont(new java.awt.Font("Snap ITC", 3, 30)); // NOI18N
        txtReto5.setForeground(new java.awt.Color(0, 0, 0));
        txtReto5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        txtReto5.setText("RETO 5");
        jPanel1.add(txtReto5, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 580, -1));

        txtEnunciado.setBackground(new java.awt.Color(204, 204, 204));
        txtEnunciado.setFont(new java.awt.Font("Yu Gothic UI Semibold", 0, 22)); // NOI18N
        txtEnunciado.setForeground(new java.awt.Color(255, 255, 255));
        txtEnunciado.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        txtEnunciado.setText("INFORMES A CONSULTAR");
        jPanel1.add(txtEnunciado, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 80, 580, -1));

        jPanel2.setBackground(new java.awt.Color(204, 204, 255));
        jPanel2.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel2.setForeground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 594, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 54, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 600, 60));

        btnInforme3.setBackground(new java.awt.Color(204, 204, 255));
        btnInforme3.setFont(new java.awt.Font("Yu Gothic", 1, 14)); // NOI18N
        btnInforme3.setForeground(new java.awt.Color(0, 0, 0));
        btnInforme3.setText("TERCER INFORME");
        btnInforme3.setFocusPainted(false);
        btnInforme3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnInforme3ActionPerformed(evt);
            }
        });
        jPanel1.add(btnInforme3, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 200, -1, -1));

        btnInforme1.setBackground(new java.awt.Color(204, 204, 255));
        btnInforme1.setFont(new java.awt.Font("Yu Gothic", 1, 14)); // NOI18N
        btnInforme1.setForeground(new java.awt.Color(0, 0, 0));
        btnInforme1.setText("PRIMER INFORME");
        btnInforme1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnInforme1.setFocusPainted(false);
        btnInforme1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnInforme1ActionPerformed(evt);
            }
        });
        jPanel1.add(btnInforme1, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 150, -1, -1));

        btnInforme2.setBackground(new java.awt.Color(204, 204, 255));
        btnInforme2.setFont(new java.awt.Font("Yu Gothic", 1, 14)); // NOI18N
        btnInforme2.setForeground(new java.awt.Color(0, 0, 0));
        btnInforme2.setText("SEGUNDO INFORME");
        btnInforme2.setFocusPainted(false);
        btnInforme2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnInforme2ActionPerformed(evt);
            }
        });
        jPanel1.add(btnInforme2, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 150, -1, -1));
        jPanel1.add(Icon3, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 210, -1, -1));
        jPanel1.add(Icon1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 150, -1, -1));
        jPanel1.add(Icon2, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 150, -1, -1));

        jLabel2.setBackground(new java.awt.Color(255, 255, 255));
        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Captura1.PNG"))); // NOI18N
        jLabel2.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 360, 190, 40));

        jLabel1.setBackground(new java.awt.Color(255, 255, 255));
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/g-banner-100-1637072459548.jpg"))); // NOI18N
        jLabel1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 260, 600, 140));

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/folder.png"))); // NOI18N
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 150, -1, -1));

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/folder.png"))); // NOI18N
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 150, -1, -1));

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/folder.png"))); // NOI18N
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 200, -1, -1));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 600, 400));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnInforme2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnInforme2ActionPerformed
        new Informe2View().setVisible(true);
    }//GEN-LAST:event_btnInforme2ActionPerformed

    private void btnInforme3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnInforme3ActionPerformed
        new Informe3View().setVisible(true);
    }//GEN-LAST:event_btnInforme3ActionPerformed

    private void btnInforme1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnInforme1ActionPerformed
        new Informe1View().setVisible(true);
    }//GEN-LAST:event_btnInforme1ActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        cerrar();
    }//GEN-LAST:event_formWindowClosing

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Icon1;
    private javax.swing.JLabel Icon2;
    private javax.swing.JLabel Icon3;
    private javax.swing.JButton btnInforme1;
    private javax.swing.JButton btnInforme2;
    private javax.swing.JButton btnInforme3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel txtEnunciado;
    private javax.swing.JLabel txtReto5;
    // End of variables declaration//GEN-END:variables
}
