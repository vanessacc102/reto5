package com.mycompany.reto5.model.vo;
/*
Generar un informe basándose en la tabla con la información respectiva al “Líder”. 
El listado debe contener: el ID_Lider, el Nombre, el Primer_Apellido y la Ciudad_Residencia. 
Este informe debe estar ordenado por la “Ciudad_Residencia” de forma alfabética.
*/

public class Informe1Vo {
    // Atributos
    private Integer idLider;
    private String nombre;
    private String primerApellido;
    private String ciudadResidencia;
    
    // Constructor
    public Informe1Vo(){
        
    }
    
    public Informe1Vo(Integer idLider, String nombre, String primerApellido, String ciudadResidencia){
        this.idLider = idLider;
        this.nombre = nombre;
        this.primerApellido = primerApellido;
        this.ciudadResidencia = ciudadResidencia;
    }

    // GETTERS
    public Integer getIdLider() {
        return idLider;
    }

    public String getNombre() {
        return nombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public String getCiudadResidencia() {
        return ciudadResidencia;
    }

    // SETTERS
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public void setCiudadResidencia(String ciudadResidencia) {
        this.ciudadResidencia = ciudadResidencia;
    }    
}
