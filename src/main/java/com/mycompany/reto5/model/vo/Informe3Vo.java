package com.mycompany.reto5.model.vo;

/*
Realizar un informe basándose en las compras realizadas por los proyectos 
con el proveedor “Homecenter” y para la ciudad “Salento”. 
Este informe debe incluir: ID_Compra, Constructora y Banco_Vinculado.
*/

public class Informe3Vo {
    // ATRIBUTOS
    private Integer idCompra;
    private String constructora;
    private String bancoVinculado;
    
    // CONSTRUCTOR
    public Informe3Vo(){
        
    }
    
    public Informe3Vo(Integer idCompra, String constructora, String bancoVinculado){
        this.idCompra = idCompra;
        this.constructora = constructora;
        this.bancoVinculado = bancoVinculado;
    }
    
    //GETTERS
    public Integer getIdCompra() {
        return idCompra;
    }

    public String getConstructora() {
        return constructora;
    }

    public String getBancoVinculado() {
        return bancoVinculado;
    }
    
    //SETTERS
    public void setConstructora(String constructora) {
        this.constructora = constructora;
    }

    public void setBancoVinculado(String bancoVinculado) {
        this.bancoVinculado = bancoVinculado;
    }
}
