package com.mycompany.reto5.model.vo;

/*
Realizar un informe basándose en la información de los proyectos cuya 
clasificación sea “Casa Campestre” y que estén ubicados en las ciudades de 
“Santa Marta”, “Cartagena” y “Barranquilla”. Este informe debe contener: 
el ID_Proyecto, la Constructora, el Nùmero_Habitaciones y la respectiva Ciudad.
*/

public class Informe2Vo {
    // ATRIBUTOS
    private Integer idProyecto;
    private String constructora;
    private Integer numHabitaciones;
    private String ciudad;
    
    // CONSTRUCTOR
    public Informe2Vo(){
        
    }
    
    public Informe2Vo (Integer idProyecto, String constructora, Integer numHabitaciones, String ciudad){
        this.idProyecto = idProyecto;
        this.constructora = constructora;
        this.numHabitaciones = numHabitaciones;
        this.ciudad = ciudad;
    }
    
    // GETTERS
    public Integer getIdProyecto() {
        return idProyecto;
    }

    public String getConstructora() {
        return constructora;
    }

    public Integer getNumHabitaciones() {
        return numHabitaciones;
    }

    public String getCiudad() {
        return ciudad;
    }
    
    // SETTERS
    public void setConstructora(String constructora) {
        this.constructora = constructora;
    }

    public void setNumHabitaciones(Integer numHabitaciones) {
        this.numHabitaciones = numHabitaciones;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }
}
