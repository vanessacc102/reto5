package com.mycompany.reto5.model.dao;

import com.mycompany.reto5.model.vo.Informe3Vo;
import com.mycompany.reto5.util.JDBCUtilities;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Informe3Dao {
    public ArrayList<Informe3Vo> listar() throws SQLException {
        ArrayList<Informe3Vo> resultado = new ArrayList<>();
        Connection conn = JDBCUtilities.getConnection();
        
        try {
            String query = "SELECT c.ID_Compra, p.Constructora, p.Banco_Vinculado FROM Proyecto p \n" +
                           "INNER JOIN Compra c ON c.ID_Proyecto = p.ID_Proyecto \n" +
                           "WHERE c.Proveedor = 'Homecenter'\n" +
                           "AND p.Ciudad = 'Salento';";
            Statement st = conn.createStatement();
            ResultSet rst = st.executeQuery(query);
            
            while (rst.next()) {
                Informe3Vo informe = new Informe3Vo(
                        rst.getInt("ID_Compra"), 
                        rst.getString("Constructora"),
                        rst.getString("Banco_Vinculado")
                );
                
                resultado.add(informe);    
            }
            
            st.close();
            rst.close();           
            
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
        
        return resultado;
    }
}
