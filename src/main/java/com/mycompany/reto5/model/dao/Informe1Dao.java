package com.mycompany.reto5.model.dao;

import com.mycompany.reto5.model.vo.Informe1Vo;
import com.mycompany.reto5.util.JDBCUtilities;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Informe1Dao {
    
    public ArrayList<Informe1Vo> listar() throws SQLException{
        ArrayList<Informe1Vo> resultado = new ArrayList<>();
        Connection conn = JDBCUtilities.getConnection();
        
        try {
            String query = "SELECT ID_Lider, Nombre, Primer_Apellido, Ciudad_Residencia\n " +
                           "FROM Lider\n " +
                           "ORDER BY Ciudad_Residencia ASC;";
            Statement st = conn.createStatement();
            ResultSet rst = st.executeQuery(query);
            
            while (rst.next()) {                
                Informe1Vo informe = new Informe1Vo(
                        rst.getInt("ID_Lider"), 
                        rst.getString("Nombre"), 
                        rst.getString("Primer_Apellido"), 
                        rst.getString("Ciudad_Residencia")
                );
                resultado.add(informe);
            }
            rst.close();
            st.close();
            
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
        return resultado;
    }
    
}
