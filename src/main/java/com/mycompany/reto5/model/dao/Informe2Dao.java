package com.mycompany.reto5.model.dao;

import com.mycompany.reto5.model.vo.Informe2Vo;
import com.mycompany.reto5.util.JDBCUtilities;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Informe2Dao {
    public ArrayList<Informe2Vo> listar() throws SQLException{
        ArrayList<Informe2Vo> resultado = new ArrayList<>();
        Connection conn = JDBCUtilities.getConnection();
        
        try {
            String query = "SELECT ID_Proyecto, Constructora, Numero_Habitaciones, Ciudad\n " +
                           "FROM Proyecto\n " +
                           "WHERE Ciudad IN ('Santa Marta', 'Cartagena','Barranquilla') \n" +
                           "AND Clasificacion = 'Casa Campestre';";
            Statement st = conn.createStatement();
            ResultSet rst = st.executeQuery(query);
            
            while (rst.next()) {                
                Informe2Vo informe = new Informe2Vo(
                        rst.getInt("ID_Proyecto"), 
                        rst.getString("Constructora"), 
                        rst.getInt("Numero_Habitaciones"), 
                        rst.getString("Ciudad")
                );
                
                resultado.add(informe);
            }
            
            st.close();
            rst.close();
            
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
        
        return resultado;
    }  
}
