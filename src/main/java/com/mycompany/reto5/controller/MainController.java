package com.mycompany.reto5.controller;

import com.mycompany.reto5.model.dao.Informe1Dao;
import com.mycompany.reto5.model.dao.Informe2Dao;
import com.mycompany.reto5.model.dao.Informe3Dao;
import com.mycompany.reto5.model.vo.Informe1Vo;
import com.mycompany.reto5.model.vo.Informe2Vo;
import com.mycompany.reto5.model.vo.Informe3Vo;
import java.sql.SQLException;
import java.util.ArrayList;

public class MainController {
    // ATRIBUTOS
    private final Informe1Dao primerInformeDao;
    private final Informe2Dao segundoInformeDao;
    private final Informe3Dao tercerInformeDao;
    
    // CONSTRUCTOR
    public MainController(){
        primerInformeDao = new Informe1Dao();
        segundoInformeDao = new Informe2Dao();
        tercerInformeDao = new Informe3Dao();
    }
    
    public ArrayList<Informe1Vo> consultarPrimerInforme() throws SQLException{
        return primerInformeDao.listar();
    }
    
    public ArrayList<Informe2Vo> consultarSegundoInforme() throws SQLException{
        return segundoInformeDao.listar();
    }
    
    public ArrayList<Informe3Vo> consultarTercerInforme() throws SQLException{
        return tercerInformeDao.listar();
    }
}
